@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-dark mb-3">Go Back</a>
    <div class="card card-body text-center">
        <h1 class="mb-3">{{$post->title}}</h1>
        <img class="mx-auto d-block" style="width:100%" src="/storage/cover_images/{{$post->cover_image}}">
        <br>
        <br>
        <div>
            {!!$post->body!!}
        </div>
        <small class="card-footer text-muted">Written on {{$post->created_at}} by {{$post->user->name}}</small>
    </div>
    @if (!Auth::guest())
        @if(Auth::user()->id == $post->user_id)
            <a href="/posts/{{$post->id}}/edit" class="btn btn-info mt-3">Edit</a>
            {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'float-right mt-3'])!!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete',['class' => 'btn btn-danger'])}}
            {!!Form::close()!!}
        @endif
    @endif
@endsection
