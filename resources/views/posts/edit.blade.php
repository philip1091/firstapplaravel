@extends('layouts.app')

@section('content')
    <h3 class="mb-3 text-center">Edit Post</h3>
    <div class="d-flex justify-content-center">
        {!! Form::open(['action' => ['PostsController@update', $post->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
            <div class="form-group ">
                {{Form::label('title', 'Title')}}
                {{Form::text('title', $post->title,['class' => 'form-control ', 'placeholder' => 'Title'])}}
            </div>
            <div class="form-group">
                {{Form::label('body', 'Body')}}
                {{Form::textarea('body', $post->body,['id'=>'article-ckeditor', 'class' => 'form-control ', 'placeholder' => 'Body text'])}}
            </div>
            <div class="form-group">
                {{Form::file('cover_image')}}
            </div>
            <div class="text-center">
                {{Form::hidden('_method','PUT')}}
                {{Form::submit('Submit', ['class'=>'btn btn-dark'])}}
            </div>
        {!! Form::close() !!} 
    </div> 
    @endsection