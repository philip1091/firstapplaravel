@extends('layouts.app')

@section('content')
    <h3 class="mb-3">Posts</h3>
    @if(count($posts) > 0)
        @foreach ($posts as $post)
            <div class="card card-body bg-light mb-4">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <img class="mx-auto d-block" style="width:100%" src="/storage/cover_images/{{$post->cover_image}}">
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <a href="/posts/{{$post->id}}">
                            <h4 class="text-dark"> {{$post->title}} </h4>
                        </a>
                        <p class="text-dark">{!!$post->body!!}</p>
                        <small class="text-dark"> Written on {{$post->created_at}} by {{$post->user->name}}</small>
                    </div>
                </div>
            </div>
        @endforeach
        {{$posts->links()}}
    @else
        <p>No Posts Found</p>
    @endif
@endsection