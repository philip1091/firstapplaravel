<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //table Name
    protected $table = 'posts';
    //primary Key
    public $primaryKey = 'id';
    //primary Key
    public $timestamps = true;

    public function user() {
        return $this->belongsTo('App\User');
    }
}
    